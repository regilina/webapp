package com.kamaleeva.webapp.models;

public class Song {

    private int id;
    private String name;
    private String path;
    private int albumId;

    public Song(String name, String path, int albumId) {
        this.name = name;
        this.path = path;
        this.albumId = albumId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

}
