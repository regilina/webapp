package com.kamaleeva.webapp.servlets;

import com.kamaleeva.webapp.AlbumDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/addAlbum")
@MultipartConfig
public class AddAlbumServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(AddAlbumServlet.class.getName());

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String albumName = request.getParameter("description");
        AlbumDAO.addAlbum(albumName);
        logger.info("Album " + albumName + " was added.");
        response.sendRedirect("/addingAlbumSaved.html");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}