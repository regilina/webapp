package com.kamaleeva.webapp.servlets;

import com.kamaleeva.webapp.SongDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@WebServlet("/addSong")
@MultipartConfig
public class AddSongServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(AddSongServlet.class.getName());

    private int albumId = 1;
    private static final String PATH_TO_MUSIC = "C://Users//rkamaleyeva//IntelliJIDEAProjects//task333//web//music//";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Part filePart = request.getPart("file");
        String fileName = getSubmittedFileName(filePart);
        Path pathTarget;
        try (InputStream fileContent = filePart.getInputStream()) {
            pathTarget = Paths.get(PATH_TO_MUSIC + fileName);
            Files.copy(fileContent, pathTarget, REPLACE_EXISTING);
        }
        SongDAO.addSong(fileName, pathTarget.toString(), albumId);
        logger.info("Song " + fileName + " was added.");
        response.sendRedirect("/addingSongSaved.html");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        albumId = Integer.parseInt(request.getParameter("albumId"));
        response.sendRedirect("/addingSong.html");
    }

    private static String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }
}
