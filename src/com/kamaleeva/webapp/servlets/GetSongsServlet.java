package com.kamaleeva.webapp.servlets;

import com.kamaleeva.webapp.SongDAO;

import java.io.IOException;

@javax.servlet.annotation.WebServlet(name = "com.kamaleeva.webapp.servlets.GetSongsServlet", urlPatterns = "/getSongs")
public class GetSongsServlet extends javax.servlet.http.HttpServlet {

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        int albumId = Integer.parseInt(request.getParameter("albumId"));
        request.setAttribute("songs", SongDAO.getSongs(albumId));
        request.getRequestDispatcher("WEB-INF/gettingSongs.jsp").forward(request, response);
    }
}