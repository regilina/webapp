package com.kamaleeva.webapp;

import com.kamaleeva.webapp.models.Song;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.kamaleeva.webapp.DAO.getConnection;

public class SongDAO {

    public static List<Song> getSongs(int albumId) {
        ArrayList<Song> songs = new ArrayList<>();

        try(Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT name, path from songs where albumId=(?)")
        ) {
            ps.setInt(1, albumId);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString(1);
                String path = resultSet.getString(2);
                songs.add(new Song(name, path,1));
            }
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return songs;
    }

    public static void addSong(String name, String path, int albumId) {
        try(Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO songs (name, path, albumId) VALUES (?, ?, ?)"))
        {
            ps.setString(1, name);
            ps.setString(2, path);
            ps.setInt(3, albumId);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
