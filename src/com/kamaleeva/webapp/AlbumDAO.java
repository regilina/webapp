package com.kamaleeva.webapp;

import com.kamaleeva.webapp.models.Album;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.kamaleeva.webapp.DAO.getConnection;

public class AlbumDAO {

    public static List<Album> getAlbums() {
        ArrayList<Album> albums = new ArrayList<>();

        try(Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement("SELECT id, name from albums");
            ResultSet resultSet = ps.executeQuery()) {

            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                albums.add(new Album(id, name));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return albums;
    }

    public static void addAlbum(String name) {
        try(Connection c = getConnection();
            PreparedStatement ps = c.prepareStatement("INSERT INTO albums (name) VALUES (?)"))
        {
            ps.setString(1, name);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
