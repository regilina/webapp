<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Music</title>
</head>

<a href="/albums">
    <img src="photo_2018-09-29_20-28-21.jpg" alt="" style="width:150px;height:150px;border:0;">
</a>

<br>
Albums
<br>
<br>
<table border="1">
    <c:forEach items="${albums}" var="album">
        <tr>
            <td>
                <a href="getSongs?albumId=${album.id}">
                    ${album.name}
                </a>
            </td>
            <td>
                <input value="upload Song" type="button"  onclick="location.href='addSong?albumId=${album.id}'"/>
            </td>
        </tr>
    </c:forEach>

</table>
</br>
<input value="Home" type="button" onclick="location.href='/albums'" />
<input value="add Album" type="button" onclick="location.href='/addingAlbum.html'" />
</body>
</html>
