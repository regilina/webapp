<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Get Songs</title>
</head>

<a href="/albums">
    <img src="photo_2018-09-29_20-28-21.jpg" alt="" style="width:150px;height:150px;border:0;">
</a>

<body>

<table border="1">
    <c:forEach items="${songs}" var="song">
       <tr><td>${song.name}</td>
        <td><audio controls>
            <source src="music/${song.name}" type="audio/mpeg">
            Your browser does not support the audio tag.
        </audio> </td></tr>

    </c:forEach>

</table>
</br>

<input value="Home" type="button" onclick="location.href='/albums'" />
</body>
</html>
